(function() {
    'use strict';

    angular
        .module('cassandraDataApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
