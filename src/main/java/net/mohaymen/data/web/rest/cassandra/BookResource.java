package net.mohaymen.data.web.rest.cassandra;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.mohaymen.data.domain.cassandra.Book;
import net.mohaymen.data.repository.cassandra.BookRepository;






@RestController
@RequestMapping("/api")
public class BookResource {
	
	@Autowired
	BookRepository bookRepository;

    private final Logger log = LoggerFactory.getLogger(BookResource.class);

   
    @GetMapping("/books")    
    public @ResponseBody List<Book> getAllBooks() {
    	List<Book> books = new ArrayList<>();
    	bookRepository.findAll().forEach(books::add);
        return books;
    }

    
}
