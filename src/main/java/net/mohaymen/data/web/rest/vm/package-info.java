/**
 * View Models used by Spring MVC REST controllers.
 */
package net.mohaymen.data.web.rest.vm;
